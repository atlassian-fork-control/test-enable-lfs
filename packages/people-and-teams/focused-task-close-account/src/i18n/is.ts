// Icelandic
export default {
  'focused-task-close-account.deactivate-account': 'Gera reikning óvirkan',
  'focused-task-close-account.delete-account': 'Eyða reikningi',
  'focused-task-close-account.learnMore': 'Fáðu að vita meira ',
  'focused-task-close-account.cancel': 'Hætta við ',
  'focused-task-close-account.next': 'Næsta',
  'focused-task-close-account.previous': 'Fyrra',
  'focused-task-close-account.delete-account.overview.heading.self':
    'Þegar reikningnum þínum er eytt',
  'focused-task-close-account.delete-account.overview.heading.admin': ' ',
  'focused-task-close-account.delete-account.overview.first.line.self':
    'Þú er við það að eyða reikningnum þínum:',
  'focused-task-close-account.delete-account.overview.first.line.admin':
    'Þú er við það að eyða reikningi:',
  'focused-task-close-account.delete-account.overview.warning-section.body':
    'Eftir 14-daga greiðslufrest muntu ekki geta hætt við eyðingu reikningsins. Ef þú heldur að þú gætir þurft á honum að halda síðar, gerðu hann þá frekar óvirkan.',
  'focused-task-close-account.delete-account.overview.warning-section.deactivated.body':
    ' ',
  'focused-task-close-account.delete-account.overview.paragraph.about-to-delete.admin':
    ' ',
  'focused-task-close-account.delete-account.overview.paragraph.about-to-delete.self':
    'Þegar þú eyðir reikningum þínum:',
  'focused-task-close-account.delete-account.overview.paragraph.loseAccess.admin':
    '{fullName} mun <b>tafarlaust missa aðgang</b> að allri Atlassian reikningsþjónustu. Í augnablikinu hafa þeir aðgang að:',
  'focused-task-close-account.delete-account.overview.paragraph.loseAccess.self':
    'Þú munt <b>tafarlaust missa aðgang</b> að allri Atlassian reikningsþjónustu. Í augnablikinu hefur þú aðgang að:',
  'focused-task-close-account.delete-account.overview.paragraph.loseAccess.admin.noSites':
    ' ',
  'focused-task-close-account.delete-account.overview.paragraph.loseAccess.self.noSites':
    'Þú munt<b>tafarlaust missa aðgang</b> að allri Atlassian reikningsþjónustu. Í augnablikinu hefur þú ekki aðgang að neinni þjónustu nema þjónustu eins og Community og Marketplace.',
  'focused-task-close-account.delete-account.overview.paragraph.loseAccess.footnote':
    'Önnur Atlassian reikningsþjónusta, eins og Atlassian Community og Marketplace.',
  'focused-task-close-account.delete-account.overview.paragraph.content-created.admin':
    'Innihaldið sem notandi hefur búið til mun vera áframhaldandi í Atlassian reikningsþjónustu.',
  'focused-task-close-account.delete-account.overview.paragraph.content-created.self':
    'Innihaldið sem þú hefur búið til mun vera áframhaldandi í Atlassian reikningsþjónustu.',
  'focused-task-close-account.delete-account.overview.inline-dialog.content-created.admin':
    'Til dæmis síður, málefni og athugasemdir sem þeir hafa búið til í vörum.',
  'focused-task-close-account.delete-account.overview.inline-dialog.content-created.self':
    'Til dæmis síður, málefni og athugasemdir sem þú hefur búið til í vörum.',
  'focused-task-close-account.delete-account.overview.paragraph.personal-data-will-be-deleted.admin':
    'Við munum <b>eyða persónuupplýsingunum þeirra</b>, eins og til dæmis fullu nafni og netfangi, af Atlassian reikningum innan 30 daga, að örfáum tilvikum undanskildum þar sem þær eru nauðsynlegar fyrir lögleg fyrirtæki og í lagalegum tilgangi.',
  'focused-task-close-account.delete-account.overview.paragraph.personal-data-will-be-deleted.self':
    'Við munum <b>eyða persónuupplýsingunum þínum</b>, eins og til dæmis fullu nafni og netfangi, af Atlassian reikningum innan 30 daga, að örfáum tilvikum undanskildum þar sem þær eru nauðsynlegar fyrir lögleg fyrirtæki og í lagalegum tilgangi.',
  'focused-task-close-account.delete-account.overview.paragraph.list-of-apps-with-personal-data.admin':
    'Við munum senda þér lista með tölvupósti af forritum sem gætu hafa geymt persónuupplýsingarnar þeirra. ',
  'focused-task-close-account.delete-account.overview.paragraph.list-of-apps-with-personal-data.self':
    'Við munum senda þér lista með tölvupósti af forritum sem gætu hafa geymt persónuupplýsingarnar þínar. ',
  'focused-task-close-account.delete-account.overview.paragraph.grace-period.admin':
    ' ',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.p1.admin':
    'Við geymum persónuupplýsingar í takmarkaðan tíma þegar um lögleg fyrirtæki eða lagalegan tilgang er að ræða. Sum þessara tilvika eru:',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.p1.self':
    'Við geymum persónuupplýsingar í takmarkaðan tíma þegar um lögleg fyrirtæki eða lagalegan tilgang er að ræða. Sum þessara tilvika eru:',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.li1.admin':
    ' ',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.li1.self':
    'Upplýsingar sem tengjast vörukaupum sem okkur er áskilið að geyma fyrir fjárhagslegar skýrslugerðir.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.li2.admin':
    'Gögn sem sýna að við höfum eytt reikningi einhvers, sem við gætum þurft að gefa upp til eftirlitsaðila.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.li2.self':
    'Gögn sem sýna að við höfum eytt reikningi einhvers, sem við gætum þurft að gefa upp til eftirlitsaðila.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.li3.admin':
    'Gögn sem eru hluti af virkri málssókn, sem okkur er áskilið að geyma samkvæmt lögum.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.li3.self':
    'Gögn sem eru hluti af virkri málssókn, sem okkur er áskilið að geyma samkvæmt lögum.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.p2.admin':
    'Við eyðum ekki neinum persónuupplýsingum úr innihaldi sem búið til var af notendum, eins og nöfnum og netföngum sem þeir slógu inn á síðu eða málefni. Vörustjórnendurnir verða að eyða þeim upplýsingum handvirkt.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.p2.self':
    'Við eyðum ekki neinum persónuupplýsingum úr innihaldi sem búið til var af þér eða öðrum aðilum, eins og nöfnum og netföngum sem slegin hafa verið inn á síðu eða málefni. Vörustjórnendurnir þínir verða að eyða þeim upplýsingum handvirkt.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.p3.admin':
    'Notendur hafa rétt á því að leggja fram kvartanir til eftirlitsstofnunar landsins síns.',
  'focused-task-close-account.delete-account.overview.inline-dialog.personal-data-will-be-deleted.p3.self':
    'Þú hefur rétt á því að leggja fram kvartanir til eftirlitsstofnunar landsins þíns.',
  'focused-task-close-account.delete-account.overview.inline-dialog.data-apps.admin':
    'Þú og aðrir notendur gætuð hafa sett upp forrit sem bæta eiginleikum við Atlassian vörur. Þessi forrit gætu hafa geymt notendasíðuupplýsingar notandans.',
  'focused-task-close-account.delete-account.overview.inline-dialog.data-apps.self':
    ' ',
  'focused-task-close-account.delete-account.content-preview.heading':
    'Hvernig notendur munu sjá þennan reikning',
  'focused-task-close-account.delete-account.drop-down-expand-button':
    '{num} meira',
  'focused-task-close-account.delete-account.drop-down-collapse-button':
    'Sýna minna ',
};
